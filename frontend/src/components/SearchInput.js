import React from 'react';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import {setSearchValue} from '../actions/actions';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    input: {
        width: 200,
    },
    div: {
        paddingLeft: 20
    }
})

const SearchInput = () => {
    
    const classes = useStyles();
    const dispatch = useDispatch();
    const searchValue = useSelector(state=>state.reducer.searchValue)

    const handleChange = (event) => {
        dispatch(setSearchValue(event.target.value))
    }

    return (
        <div className={classes.div}>
            <TextField 
                className={classes.input} 
                id="filled-basic" 
                label="Buscar..."
                onChange={handleChange}
                value={searchValue}/>
        </div>
    )
}

SearchInput.propTypes = {
	searchValue: PropTypes.string.isRequired,
}

export default SearchInput;