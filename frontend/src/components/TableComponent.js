import React, {useState} from 'react';
import { useSelector,useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TablePagination from '@material-ui/core/TablePagination';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import SearchInput from './SearchInput';
import Tooltip from '@material-ui/core/Tooltip';
import {
    Link
  } from "react-router-dom";
import {setPage} from '../actions/actions';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
    table: {
        minWidth: 650,
        maxWidth: 800,
    },
    head: {
        backgroundColor: '#2196f3',
    },
    title: {
        color: 'white'
    },
    searchInput: {
        marginTop: 20
    }
})

const TableComponent = ({characters,totalCharacters}) => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const loading = useSelector(state => state.reducer.loading);
    const page = useSelector(state => state.reducer.page);

    const handleChangePage = (event, newPage) => {
        dispatch(setPage(newPage));
    };

    const loadingData = () => {
        return(
            <div
                style={{
                    position: 'absolute', left: '50%', top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}
                >
                <CircularProgress color="primary" />
            </div>
        )
    }

    return(
        <React.Fragment>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead className={classes.head}>
                        <TableRow>
                            <TableCell className={classes.title}>Nombre</TableCell>
                            <TableCell className={classes.title}>Slug</TableCell>
                            <TableCell className={classes.title}>Género</TableCell>
                            <TableCell className={classes.title}>Casa</TableCell>
                            <TableCell className={classes.title}>Libros</TableCell>
                            <TableCell className={classes.title}>Títulos</TableCell>
                            <TableCell className={classes.title}>Acción</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading && characters.length < 1 && loadingData()}
                        <TableRow>
                            <SearchInput />
                        </TableRow>
                        {characters.map((character) => (
                            <TableRow key={character._id}>
                                <TableCell component="th" scope="row">
                                    {character.name}
                                </TableCell>
                                <TableCell >{character.slug}</TableCell>
                                <TableCell >{character.gender}</TableCell>
                                <TableCell >{character.house}</TableCell>
                                <TableCell >{character.books}</TableCell>
                                <TableCell >{character.titles}</TableCell>
                                <TableCell >
                                    <Tooltip title="Ver detalle" aria-label="detail">
                                        <IconButton component={Link} to={`/${character._id}`}>
                                            <Visibility />
                                        </IconButton>
                                    </Tooltip>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            { !loading &&
                <TablePagination
                    component="div"
                    count={totalCharacters}
                    rowsPerPage={10}
                    rowsPerPageOptions={[]}
                    page={page}
                    onChangePage={handleChangePage}
                />
            }
        </React.Fragment>
    )
}

TableComponent.propTypes = {
	page: PropTypes.number.isRequired,
	totalCharacters: PropTypes.number.isRequired,
	loading: PropTypes.bool.isRequired,
    characters: PropTypes.array.isRequired
}

export default TableComponent;