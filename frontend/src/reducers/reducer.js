import * as ActionType from '../actions/types'

const defaultState = {
    allCharacters: [],
    character: {},
    totalCharacters: 0,
    page: 0,
    loading: true,
    searchValue: '',
    error: ''
}

const reducer = (state = defaultState, action) => {
    switch(action.type){
        case ActionType.GET_ALL_CHARACTERS_REQUEST:
            return {
                ...state,
                loading: true
            }
        case ActionType.GET_ALL_CHARACTERS_SUCCESS:
            return {
                ...state,
                allCharacters: action.payload.characters,
                totalCharacters: action.payload.totalCharacters,
                loading: false
            }
        case ActionType.GET_ALL_CHARACTERS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case ActionType.GET_FILTERED_CHARACTER:
            return {
                ...state,
                filteredCharacters: state.allCharacters.filter(character => character.name.toLowerCase().search(action.payload) !== -1 || ( character.house && character.house.toLowerCase().search(action.payload) !== -1))
            } 
        case ActionType.GET_CHARACTER_SUCCESS:
            return {
                ...state,
                loading: false,
                character: action.payload
            }
        case ActionType.GET_CHARACTER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case ActionType.GET_CHARACTER_REQUEST:
            return {
                ...state,
                loading: true
            }
        case ActionType.SET_PAGE:{
            return {
                ...state,
                page: action.payload
            }
        }
        case ActionType.SET_SEARCH_VALUE:{
            return {
                ...state,
                searchValue: action.payload
            }
        }
        default:
            return state
    }
}

export default reducer;