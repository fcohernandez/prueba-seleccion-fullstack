import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Character from './containers/Characters';
import CharacterDetail from './containers/CharactersDetail';

const App = () => {
  return (
    <Router>
        <Switch>
			<Route exact={true} path="/">
				<Character />
			</Route>
			<Route path="/:id">
				<CharacterDetail />
			</Route>
        </Switch>
    </Router>
  );
}

export default App;
