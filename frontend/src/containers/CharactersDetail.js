import React, {useEffect} from 'react';
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import {getCharacter} from '../actions/actions';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types';

import {
    Link
  } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      width: 300,
      margin: 'auto'
    },
    media: {
        height: 600,
        width: '100%'
    },
  });

const CharacterDetail = () => {
    const { id } = useParams();
    const dispatch = useDispatch();
    const classes = useStyles();
    const character = useSelector(state => state.reducer.character);
    const loading = useSelector(state => state.reducer.loading);

    useEffect(() => {
        dispatch(getCharacter(id))
    },[])

    return(
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            {loading && <CircularProgress color="primary" />}
            {character &&
                <Card className={classes.root}>
                    <CardActionArea>
                        {character.image &&
                            <CardMedia
                                className={classes.media}
                                image={character.image}
                                title="Contemplative Reptile"
                            />
                        }
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {character.name}
                            </Typography>
                            <Typography variant="body2" color="textPrimary" component="p">
                                <b>Casa:</b>{character.house}
                            </Typography>
                            <Typography variant="body2" color="textPrimary" component="p">
                                <b>Género:</b>{character.gender}
                            </Typography>
                            <Typography variant="body2" color="textPrimary" component="p">
                                <b>Slug:</b>{character.slug}
                            </Typography>
                            <Typography variant="body2" color="textPrimary" component="p">
                            <b>Rango:</b>{character.pagerank?.rank}
                            </Typography>
                            {character.titles && character.titles.length > 0 && <Typography variant="body2" color="textPrimary" component="p"><b>Títulos:</b> <ol>{character.titles.map((t,index) => (<li key={t}>{t}</li>))}</ol></Typography>}
					        {character.books && character.books.length > 0	&&<Typography variant="body2" color="textPrimary" component="p"><b>Libros:</b> <ol>{character.books.map((book,index) => (<li key={book}>{book}</li>))}</ol></Typography>}
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button size="small" color="primary" component={Link} to={`/`}>
                            Volver
                        </Button>
                    </CardActions>
                </Card>
            }
        </Grid>
    )
}

CharacterDetail.propTypes = {
	loading: PropTypes.bool.isRequired,
	id: PropTypes.string.isRequired,
    characters: PropTypes.array.isRequired
}

export default CharacterDetail;