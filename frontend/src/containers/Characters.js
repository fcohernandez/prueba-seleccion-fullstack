import React, {useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import {getAllCharacters} from '../actions/actions';
import Grid from '@material-ui/core/Grid';

import TableComponent from '../components/TableComponent';

const Character = () => {
    const dispatch = useDispatch();
    const characters = useSelector(state => state.reducer.allCharacters)
    const totalCharacters = useSelector(state => state.reducer.totalCharacters)
    const page = useSelector(state => state.reducer.page)
    const searchValue = useSelector(state => state.reducer.searchValue)

    useEffect(() => {
        dispatch(getAllCharacters(searchValue,page+1))
    },[page,searchValue])

    return(
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            {characters && 
                <Grid item xs={12}>
                    <TableComponent characters={characters} totalCharacters={totalCharacters}/>
                </Grid>
            }
        </Grid>
    )
}

Character.propTypes = {
	page: PropTypes.number.isRequired,
	totalCharacters: PropTypes.number.isRequired,
	searchValue: PropTypes.string.isRequired,
    characters: PropTypes.array.isRequired
}

export default Character;