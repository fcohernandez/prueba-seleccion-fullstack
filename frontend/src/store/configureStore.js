import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from "redux-devtools-extension"
import rootReducer from '../reducers/rootReducer'

//arreglo con todos los middlewares, en este caso solo thunk
const middleware = [
    thunk,
];

export default function configureStore(initialState={}) {
    return createStore(
        //reducer principal
        rootReducer,
        //estado inicial
        initialState,
        //herramienta de debug
        composeWithDevTools(
            applyMiddleware(...middleware),
        )
    );
}