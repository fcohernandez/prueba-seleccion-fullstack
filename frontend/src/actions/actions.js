import * as ActionType from './types';
import * as Config from '../config/config';
import axios from 'axios';

//acción que se ejecuta cuando la obtención de la información de los personajes es exitosa
export const getAllCharactersSuccess = (data) => dispatch => {
    dispatch({
        type: ActionType.GET_ALL_CHARACTERS_SUCCESS,
        payload: data
    })
}

//acción que se ejecuta cuando la obtención de la información de los personajes es fallida
export const getAllCharactersFailure = (error) => dispatch => {
    dispatch({
        type: ActionType.GET_ALL_CHARACTERS_FAILURE,
        payload: error
    })
}

//acción que se ejecuta cuando se inicia la petición para obtener la información
export const getAllCharactersRequest = () => dispatch => {
    dispatch({
        type: ActionType.GET_ALL_CHARACTERS_REQUEST
    })
}

//acción para obtener las info de los personajes
export const getAllCharacters = (search,page) => async(dispatch) => {
    dispatch(getAllCharactersRequest())
    const params = {
		search,
		page
	}
    try{
        const response = await axios.get(`${Config.BASE_URL}/characters`,{params})
        if(response.data.ok){
            dispatch(getAllCharactersSuccess(response.data))
        }else{
            dispatch(getAllCharactersFailure(response.msg))
        }

    }catch(err){
        dispatch(getAllCharactersFailure(err))
    }
}

//filtrar personajes
export const getFilteredCharacters = (search) => dispatch => {
    dispatch({
        type: ActionType.GET_FILTERED_CHARACTER,
        payload: search
    })
}

//acción que se ejecuta cuando la obtención de la información del personaje es exitosa
export const getCharacterSuccess = (data) => dispatch => {
    dispatch({
        type: ActionType.GET_CHARACTER_SUCCESS,
        payload: data
    })
}

//acción que se ejecuta cuando la obtención de la información del personaje es fallida
export const getCharacterFailure = (error) => dispatch => {
    dispatch({
        type: ActionType.GET_CHARACTER_FAILURE,
        payload: error
    })
}

//acción que se ejecuta cuando se inicia la petición para obtener la información
export const getCharacterRequest = () => dispatch => {
    dispatch({
        type: ActionType.GET_CHARACTER_REQUEST
    })
}

//acción para obtener las info del personaje
export const getCharacter = (id) => async(dispatch) => {
    dispatch(getCharacterRequest())
    try{
        const response = await axios.get(`${Config.BASE_URL}/characters/${id}`)
        if(response.data.ok){
            dispatch(getCharacterSuccess(response.data.character))
        }else{
            dispatch(getCharacterFailure(response.data.msg))
        }

    }catch(err){
        dispatch(getCharacterFailure(err))
    }
}

//acción que se ejecuta para setear la pagina
export const setPage = (page) => dispatch => {
    dispatch({
        type: ActionType.SET_PAGE,
        payload: page
    })
}

//acción que se ejecuta cuando se inicia la petición para obtener la información
export const setSearchValue = (search) => dispatch => {
    dispatch({
        type: ActionType.SET_SEARCH_VALUE,
        payload: search
    })
}

