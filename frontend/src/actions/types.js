export const GET_ALL_CHARACTERS_SUCCESS = 'GET_ALL_CHARACTERS_SUCCESS'
export const GET_ALL_CHARACTERS_FAILURE = 'GET_ALL_CHARACTERS_FAILURE'
export const GET_ALL_CHARACTERS_REQUEST = 'GET_ALL_CHARACTERS_REQUEST'

export const GET_CHARACTER_SUCCESS = 'GET_CHARACTER_SUCCESS'
export const GET_CHARACTER_FAILURE = 'GET_CHARACTES_FAILURE'
export const GET_CHARACTER_REQUEST = 'GET_CHARACTER_REQUEST'

export const GET_FILTERED_CHARACTER = 'GET_FILTERED_CHARACTER'

export const SET_PAGE = 'SET_PAGE'

export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE'