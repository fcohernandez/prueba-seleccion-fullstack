const { response } = require("express");
const Character = require('../models/Character');

const getAllCharacters = async(req,res) => {

    const { page = 1, search = '' } =  req.query;
    try{
        let find = search !== '' ? { $or: [ { name: { $regex: search, $options: "i" } }, { house: { $regex: search, $options: "i" } } ] }: {};
        const [characters,totalCharacters] = await Promise.all([
            Character.find(find)
                .limit(10)
                .skip((Number(page) - 1) * 10)
                .sort('name')
                .exec(),
            Character.find(find).countDocuments()
        ])

        res.status(200).json({
            ok: true,
            characters,
            totalCharacters
        })
    }catch(err) {
        res.status(500).json({
            ok: false,
            msg: 'Ha ocurrido un error al obtener los personajes.'
        });
    }
}

const getCharacter = async(req,res = response) => {

    try{
        const {id} = req.params;

        let character = await Character.findById({_id: id});

		if( !character ){
			return res.status(404).json({
				ok: true,
				msg: 'La id ingresada no corresponde a ningún personaje.'
			});
		}

        res.status(200).json({
            ok: true,
            character
        })
    }catch(err) {
        res.status(500).json({
            ok: false,
            msg: 'Ha ocurrido un error al obtener el personaje.'
        });
    }
}

module.exports = {
    getAllCharacters,
    getCharacter
}