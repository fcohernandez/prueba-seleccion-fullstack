const { Router } = require('express');
const { getAllCharacters, getCharacter } = require('../controllers/character');

const router = Router();

router.get('/', getAllCharacters);

router.get('/:id', getCharacter);

module.exports = router;