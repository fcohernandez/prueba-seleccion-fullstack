const { Schema, model } = require('mongoose');

const CharacterSchema = Schema({
    name:{
        type: String
    },
    gender: {
		type: String
	},
		
	slug: {
		type: String 
	},
	
    pagerank: {
		type: Object 
	},
		
    house: {
		type: String
	},
	
	books: {
		type: Array
	},
	
    titles: {
		type: Array
	},

    image: {
		type: String 
	},

    createdAt: {
		type: Date, 
		default: Date.now 
	}
})


module.exports = model('Character', CharacterSchema);