const express = require("express");
const cors = require("cors");
const axios = require("axios");

const { dbConnection } = require('../database/config');
const Character = require('./Character');

class Server {

    constructor(){
        this.app = express();
        this.port = process.env.PORT;
        this.charactersPath = '/characters';

        this.connectDB();
        
        this.populateDB();

        this.middlewares();

        //Rutas app
        this.routes();
    }

    async connectDB(){
        await dbConnection();
    }

    middlewares(){

        //cors
        this.app.use(cors());

        //parsear el body
        this.app.use(express.json());

    }

    routes(){
        this.app.use(this.charactersPath, require('../routes/character'));
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log("Servido corriendo en puerto",this.port);
        })
    }

    populateDB(){
        try {

            Character.countDocuments().then( async(count) => {
                if(count === 0){
                    const response = await axios.get( process.env.API_GOTH );
            
                    const { data } = response;
                    
                    let newCharacters = data.map( character => new Character(character) );
        
                    Character.create( newCharacters )
                        .then(res => {
                            console.log('DB poblada');
                        })
                        .catch(err => {
                            console.log('Ha ocurrido un error al poblar la db');
                        });
                }else{
                    console.log("La DB ya ha sido poblada");
                }

            }).catch(err => {
                console.log("Hubo un error al obtener la data",err);
            })

        } catch (error) {
            console.log("Ha ocurrido un error",error);
        }
        
    }
}

module.exports = Server;